package com.hendisantika.easynotes.repository;

import com.hendisantika.easynotes.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : easy-notes
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/11/19
 * Time: 22.23
 */

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {
    List<Note> findByTitle(String title);
}