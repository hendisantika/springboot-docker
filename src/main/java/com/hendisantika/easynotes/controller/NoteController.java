package com.hendisantika.easynotes.controller;

import com.hendisantika.easynotes.model.Note;
import com.hendisantika.easynotes.repository.NoteRepository;
import com.hendisantika.easynotes.service.INotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : easy-notes
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/11/19
 * Time: 22.29
 */
@RestController
@RequestMapping("/api")
public class NoteController {

    @Autowired
    NoteRepository noteRepository;

    @Autowired
    INotesService noteService;

    // Get All Notes
    @GetMapping("/getNotes")
    //@RequestMapping(value="/notes", method=RequestMethod.Post) above is its short form
    public List<Note> getAllNotes() {
        return noteService.getAllNotes();
    }

    // Create a new Note
    @PostMapping("/createNote")
    public Note createNote(@Valid @RequestBody Note note) {
        return noteService.addNote(note);
    }

    //Get single note
    @GetMapping("/getById/{id}")
    public ResponseEntity<Note> getNoteById(@PathVariable(value = "id") Long noteId) {

        Optional<Note> noteOptional = noteService.getNoteById(noteId);
        if (!noteOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(noteOptional.get());
    }

    //update a note
    @PutMapping("/updateNote/{id}")
    public ResponseEntity<Note> updateNote(@PathVariable(value = "id") Long noteId, @Valid @RequestBody Note noteDetails) {

        Optional<Note> noteOptional = noteService.getNoteById(noteId);
        if (!noteOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        noteOptional.get().setTitle(noteDetails.getTitle());
        noteOptional.get().setContent(noteDetails.getContent());

        Note updateNote = noteService.addNote(noteOptional.get());
        return ResponseEntity.ok(updateNote);
    }

    //delete a note
    @DeleteMapping("/deleteNote/{id}")
    public ResponseEntity<Note> deleteNote(@PathVariable(value = "id") Long noteId) {

        Optional<Note> noteOptional = noteService.getNoteById(noteId);
        if (!noteOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        noteService.delete(noteOptional.get());
        return ResponseEntity.ok().build();
    }
}
