package com.hendisantika.easynotes.service;

import com.hendisantika.easynotes.model.Note;
import com.hendisantika.easynotes.repository.NoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : easy-notes
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/11/19
 * Time: 22.28
 */

@Service
public class NoteServiceImpl implements INotesService {

    private final static Logger LOG = LoggerFactory.getLogger(NoteServiceImpl.class);

    @Autowired
    NoteRepository noteRepository;

    @Override
    public List<Note> getAllNotes() {
        List<Note> getNotes = noteRepository.findAll();
        LOG.debug("All Notes:{}", getNotes);
        return getNotes;
    }

    @Override
    public Note addNote(Note note) {
        Note save = noteRepository.save(note);
        LOG.debug("Saved Note:{}", note);
        return save;
    }

    @Override
    public Optional<Note> getNoteById(Long id) {
        return noteRepository.findById(id);
    }

    @Override
    public void delete(Note note) {
        noteRepository.delete(note);
    }

}