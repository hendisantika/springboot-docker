package com.hendisantika.easynotes.service;

import com.hendisantika.easynotes.model.Note;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : easy-notes
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/11/19
 * Time: 22.27
 */
public interface INotesService {
    List<Note> getAllNotes();

    Note addNote(Note note);

    Optional<Note> getNoteById(Long id);

    void delete(Note note);
}