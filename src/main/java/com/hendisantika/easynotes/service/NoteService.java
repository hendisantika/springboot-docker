package com.hendisantika.easynotes.service;

import org.springframework.context.annotation.Profile;

/**
 * Created by IntelliJ IDEA.
 * Project : easy-notes
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/11/19
 * Time: 22.25
 */
@org.springframework.stereotype.Service
@Profile("test1")
public interface NoteService {

}